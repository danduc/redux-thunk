import axios from 'axios';
import regeneratorRuntime from "regenerator-runtime";
import _ from 'lodash';

const config = {
    headers:{ "Content-Type": "application/json; charset=UTF-8" }
};

const _fetchUser = _.memoize(async (id,dispatch) => 
{
    const response = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`, config);
    
    return dispatch({ type : 'FETCH_USER', payload : response.data});
});

/***** Actions Start *****/

export const fetchPosts = () => async dispatch => 
{
    const response = await axios.get('https://jsonplaceholder.typicode.com/posts', config);
    
    return dispatch({ type : 'FETCH_POSTS', payload : response.data});
};

export const fetchUser = (id) => dispatch => _fetchUser(id,dispatch);

/***** Actions End *****/